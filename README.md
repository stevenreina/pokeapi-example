# PokéAPI example

Just a litte example of the [PokéAPI](https://pokeapi.co).


## Preview
```
$ git clone https://gitlab.com/stevenreina/pokeapi-example
$ cd pokeapi-example
$ npm i
$ npm start
```
Open the link that appears, and press the 'q' key when done.

1. ```$``` means "command to be run on the terminal".
2. ```nodejs``` is required.
