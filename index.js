
const MAX_POKEMON_ID = 151;

function upperInitial (str) {
  return str
    .split('')
    .map((c, i) => (i === 0 ? c.toUpperCase() : c))
    .join('');
}

var currentPokemonId = 0;
function setPokemonData (pokemon) {
  const sprite = document.querySelector('img');
  const name = document.querySelector('#pokemon-name');
  const height = document.querySelector('#height');
  const weight = document.querySelector('#weight');
  const types = document.querySelector('#types');
  const moves = document.querySelector('#moves');

  sprite.setAttribute('src', pokemon.sprite);
  name.innerText = pokemon.name;
  name.setAttribute('pokeId', ` #${pokemon.id}`);
  height.innerText = pokemon.height;
  weight.innerText = pokemon.weight;

  types.replaceChildren('');
  pokemon.types.forEach((type) => {
    const typeElement = document.createElement('span');
    typeElement.className = `type ${type.toLowerCase()}`;
    typeElement.innerText = type;
    types.appendChild(typeElement);
  });

  moves.replaceChildren('');
  pokemon.moves.forEach((move) => {
    const moveElement = document.createElement('span');
    moveElement.className = 'move';
    moveElement.innerText = upperInitial(move.name);
    moves.appendChild(moveElement);
  });
  currentPokemonId = pokemon.id;
}

function setPokemon (pokeId) {
  fetch(`https://pokeapi.co/api/v2/pokemon/${pokeId}/`)
    .then((res) => res.json())
    .then((res) => {
      const pokemon = {};
      pokemon.name = upperInitial(res.name);
      pokemon.id = pokeId;
      pokemon.sprite = res.sprites.front_default;
      pokemon.height = res.height;
      pokemon.weight = res.weight;
      pokemon.types = res.types.map((typeObj) =>
        upperInitial(typeObj.type.name)
      );
      pokemon.moves = res.moves.map((moveObj) => moveObj.move);
      setPokemonData(pokemon);
    })
    .catch(console.log);
}

document.querySelector('#randomize').addEventListener('click', () => {
  const randPokeId = Math.floor(Math.random() * MAX_POKEMON_ID) + 1;
  setPokemon(randPokeId);
});

document.querySelector('#previous').addEventListener('click', () => {
  if (currentPokemonId == 1) {
    setPokemon(MAX_POKEMON_ID);
  } else {
    setPokemon(currentPokemonId - 1);
  }
});

document.querySelector('#next').addEventListener('click', () => {
  if (currentPokemonId == MAX_POKEMON_ID) {
    setPokemon(1);
  } else {
    setPokemon(currentPokemonId + 1);
  }
});

var isInfoSectionVisible = false;
function updateInfoSectionDisplay () {
  const mainSection = document.querySelector('#main-section');
  const infoSection = document.querySelector('#info-section');
  const showMovesButton = document.querySelector('#show-info-section');

  if (isInfoSectionVisible) {
    mainSection.style['transform'] = 'translateX(-53%)';
    infoSection.style['transform'] = 'translateX(53%)';
    infoSection.style['opacity'] = '1';
    showMovesButton.innerHTML = '◄';
  } else {
    mainSection.style['transform'] = 'translateX(0)';
    infoSection.style['transform'] = 'translateX(0)';
    infoSection.style['opacity'] = '0';
    showMovesButton.innerHTML = '►';
  }
}

document.querySelector('#show-info-section').addEventListener('click', () => {
  isInfoSectionVisible = !isInfoSectionVisible;
  updateInfoSectionDisplay();
});

var isOpen = {
  'moves': true
};
function updateDataDisplay () {
  for (const [label, open] of Object.entries(isOpen)) {
    const targetObj = document.querySelector(`#${label}`);
    if (open) {
      targetObj.style['display'] = 'flex';
    } else {
      targetObj.style['display'] = 'none';
    }
  }
}

document.querySelectorAll('#info-section>div>div:first-child')
  .forEach( (elem) => elem.addEventListener('click', function (evt) {
    const target = evt.currentTarget.innerHTML.toLowerCase();
    isOpen[target] = !isOpen[target];
    updateDataDisplay();
  }));

updateInfoSectionDisplay();
updateDataDisplay();
